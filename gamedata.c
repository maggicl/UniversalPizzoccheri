#include <stdio.h>
#include "gamedata.h"

struct _gamedata {
  double chf;
  int bitto;
  int flour;
  int pizzoccheri;
  double chf_unit;
};

gamedata_t* gamedata_new() {
  gamedata_t* self = (gamedata_t*) malloc(sizeof(gamedata_t));
  if(errno != 0) {
    perror("gamedata malloc() creation error: ");
  }

  srand(time(NULL));

  self->chf = 0;
  self->bitto = 200;
  self->flour = 400;
  self->pizzoccheri = 0;
  self->chf_unit = 0.5;
  return self;
}

void gamedata_delete(gamedata_t* self) {
  free(self);
}

void gamedata_print_status(gamedata_t *self, FILE* out) {
  fprintf(out, "OK BITTO %d, FLOUR %d, ", self->bitto, self->flour);
  fprintf(out, "INVENTORY %d, ", self->pizzoccheri);
  fprintf(out, "UNIT PRICE %.02f CHF, PROFIT %.02f CHF\n", self->chf_unit, self->chf);
}

#define Q_BITTO_UNIT 1
#define Q_FLOUR_UNIT 2
bool gamedata_make_pizzoccheri(gamedata_t *self, unsigned int q) {
  int q_flour = q * Q_FLOUR_UNIT;
  int q_bitto = q * Q_BITTO_UNIT;

  if(self->flour < q_flour || self->bitto < q_bitto) {
    return false;
  }

  self->flour -= q_flour;
  self->bitto -= q_bitto;
  self->pizzoccheri += q;

  return true;
}

#define PRICE_BITTO_UNIT 0.5
bool gamedata_buy_bitto(gamedata_t *self, unsigned int q) {
  double price = q * PRICE_BITTO_UNIT;

  if(price > self->chf) {
    return false;
  }

  self->chf -= price;
  self->bitto += q;
  return true;
}

#define PRICE_FLOUR_UNIT 0.5
bool gamedata_buy_flour(gamedata_t *self, unsigned int q) {
  double price = q * PRICE_FLOUR_UNIT;

  if(price > self->chf) {
    return false;
  }

  self->chf -= price;
  self->flour += q;
  return true;
}

unsigned int gamedata_sell_pizzoccheri(gamedata_t *self) {
  if(self->pizzoccheri == 0) {
    return 0;
  }

  int sold = 5 * (-self->chf_unit + 1 + 2 * (double)rand() / (double)RAND_MAX);

  if(sold <= 0) {
    return 0;
  }
  if(sold > self->pizzoccheri) {
    sold = self->pizzoccheri;
  }

  self->pizzoccheri -= sold;
  self->chf += sold * self->chf_unit;

  return sold;
}

bool gamedata_set_unit_price(gamedata_t *self, double price) {
  if(price < 0) return false;
  self->chf_unit = price;
  return true;
}

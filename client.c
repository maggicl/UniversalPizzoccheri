#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>
#include <arpa/inet.h>
#include <errno.h>

#define MAXSIZE 100

static struct termios old, new;

/* Initialize new terminal i/o settings */
void initTermios(int echo)
{
  tcgetattr(0, &old); /* grab old terminal i/o settings */
  new = old; /* make new settings same as old settings */
  new.c_lflag &= ~ICANON; /* disable buffered i/o */
  new.c_lflag &= echo ? ECHO : ~ECHO; /* set echo mode */
  tcsetattr(0, TCSANOW, &new); /* use these new terminal i/o settings now */
}

/* Restore old terminal i/o settings */
void resetTermios(void)
{
  tcsetattr(0, TCSANOW, &old);
}

/* Read 1 character - echo defines echo mode */
char getch_(int echo)
{
  char ch;
  initTermios(echo);
  ch = getchar();
  resetTermios();
  return ch;
}

/* Read 1 character without echo */
char getch(void)
{
  return getch_(0);
}

static void titolo(){
  printf(" _    _         _                                _   _____  _                             _                  _ \n| |  | |       (_)                              | | |  __ \\(_)                           | |                (_)\n| |  | | _ __   _ __   __ ___  _ __  ___   __ _ | | | |__) |_  ____ ____ ___    ___  ___ | |__    ___  _ __  _ \n| |  | || '_ \\ | |\\ \\ / // _ \\| '__|/ __| / _` || | |  ___/| ||_  /|_  // _ \\  / __|/ __|| '_ \\  / _ \\| '__|| |\n| |__| || | | || | \\ V /|  __/| |   \\__ \\| (_| || | | |    | | / /  / /| (_) || (__| (__ | | | ||  __/| |   | |\n \\____/ |_| |_||_|  \\_/  \\___||_|   |___/ \\__,_||_| |_|    |_|/___|/___|\\___/  \\___|\\___||_| |_| \\___||_|   |_|\n---------------------------------------------------------------------------------------------------------------\n\n");


}

int menu() {
  int c,x=0;
  do{
      printf( "\e[2J\e[H" );
      titolo();
      printf("+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +\n");
    printf("|                                                                                                             |\n");
    printf("|                                        Decidi cosa fare                                                     |\n");
    printf("|                                                                                                             |\n");
    printf("+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +\n");
    printf("|                                                                                                             |\n");
    printf("|                                                                                                             |\n");
      if(x==0) printf("|   (1) Fare i pizzocheri                                                                                     |\n|       -----------------");
      else     printf("|    1  Fare i pizzocheri                                                                                     |\n|                        ");
      printf("                                                                                     |\n");

      if(x==1) printf("|   (2) Vendere i pizzocheri                                                                                  |\n|       --------------------");
      else     printf("|    2  Vendere i pizzocheri                                                                                  |\n|                           ");
      printf("                                                                                  |\n");

      if(x==2) printf("|   (3) Cambiare prezzo                                                                                       |\n|       ---------------");
      else     printf("|    3  Cambiare prezzo                                                                                       |\n|                      ");
      printf("                                                                                       |\n");

      if(x==3) printf("|   (4) Comprare il bitto                                                                                     |\n|       -----------------");
      else     printf("|    4  Comprare il bitto                                                                                     |\n|                        ");
      printf("                                                                                     |\n");

      if(x==4) printf("|   (5) Comprare la farina                                                                                    |\n|       ------------------");
      else     printf("|    5  Comprare la farina                                                                                    |\n|                         ");
      printf("                                                                                    |\n");

      if(x==5) printf("|   (6) Esci                                                                                                  |\n|       ----");
      else     printf("|    6  Esci                                                                                                  |\n|           ");
      printf("                                                                                                  |\n");

      for(int i=0;i<4;i++)printf("|                                                                                                             |\n");
      printf("+-------------------------------------------------------------------------------------------------------------+\n");
      printf(" Usare 1-6 o +/- per spostarsi e \\n per selezionare.");

    c = (int)getch();
    if(c>=49 && c<=54) x=c-49;
    if(c==45 && x<6) x++;
    if(c==45 && x==6) x=0;
    if(c==43 && x>=0) x--;
    if(c==43 && x==-1) x=5;
  } while(c != '\n');

  return x;
}


int main(int argc, char *argv[])
{
  char buffer[MAXSIZE];
  char messaggio[MAXSIZE];            // messaggio da inviare
  struct sockaddr_in locale, remoto;
  int socketfd;                       // identificatore della socket

  if(argc < 3) {
    fprintf(stderr, "Inserire l'indirizzo come primo argomento e la porta come secondo\n");
    exit(255);
  }

  // impostazione del transport endpoint
  printf ("Connessione col server\n");
  socketfd = socket(AF_INET, SOCK_STREAM, 0);
  if (socketfd == -1) {
    perror("ERRORE");
    exit(1);
  }

  memset (&locale, 0, sizeof(locale));
  locale.sin_family	      =	AF_INET;
  locale.sin_addr.s_addr	=	htonl(INADDR_ANY);
  locale.sin_port	        =	htons(0);
  int ris = bind(socketfd, (struct sockaddr*) &locale, sizeof(locale));
  if (ris == -1) {
    perror("ERRORE");
    exit(3);
  }

  // assegnazione parametri del server
  memset (&remoto, 0, sizeof(remoto));
  remoto.sin_family	     = AF_INET;
  remoto.sin_addr.s_addr = inet_addr(argv[1]);
  remoto.sin_port		     = htons(atoi(argv[2]));

  //connessione
  ris = connect(socketfd, (struct sockaddr*) &remoto, sizeof(remoto));
  if (ris == -1) {
    perror("fallimento di connect(): ");
    exit(4);
  }

  FILE* in = fdopen(socketfd, "r");
  FILE* out = fdopen(socketfd, "w");
  int scelta = -1;

  while(true) {
    switch(scelta = menu()) {
    case 0:
      printf("\nInserisci quanti pizzoccheri produrre (intero positivo): ");
      unsigned int pizz;
      scanf("%u", &pizz);
      sprintf(messaggio, "make %d\n", pizz);
      break;
    case 1:
      strcpy(messaggio, "sell\n");
      printf("\n");
      break;
    case 2:
      printf("\nInserisci nuovo prezzo in CHF (decimale): ");
      double prezzo;
      scanf("%lf",&prezzo);
      sprintf(messaggio, "set price %lf\n", prezzo);
      break;
    case 3:
      printf("\nInserisci quanto bitto comprare (intero positivo): ");
      unsigned int bitto;
      scanf("%u",&bitto);
      sprintf(messaggio, "buy bitto %d\n", bitto);
      break;
    case 4:
      printf("\nInserisci quanta farina comprare (intero positivo): ");
      unsigned int farina;
      scanf("%u",&farina);
      sprintf(messaggio, "buy flour %d\n", farina);
      break;
    case 5:
      printf("\nChiusura in corso...");
      fprintf(out, "exit\n");
      fclose(in);
      fclose(out);
      exit(0);
    default:
      continue;
    }

    int n = fprintf(out, messaggio);
    if(n <= 0) {
      perror("errore operazione di write()");
      exit(5);
    }
    fflush(out);

    fgets(buffer, MAXSIZE, in);
    printf("server: %s\n", buffer);
    fflush(stdout);

    printf("Premere un tasto per continuare...");
    if(scelta != 1) getch(); // pulisci buffer da invio della scanf
    getch();
  }
}

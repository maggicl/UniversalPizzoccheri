#ifndef gamedata
#define gamedata
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>

typedef struct _gamedata gamedata_t;

extern gamedata_t* gamedata_new();
extern void gamedata_delete();
extern bool gamedata_buy_bitto(gamedata_t*, unsigned int);
extern bool gamedata_buy_flour(gamedata_t*, unsigned int);
extern void gamedata_print_status(gamedata_t*, FILE*);
extern bool gamedata_make_pizzoccheri(gamedata_t*, unsigned int);
extern unsigned int gamedata_sell_pizzoccheri(gamedata_t*);
extern bool gamedata_set_unit_price(gamedata_t*, double);

#endif

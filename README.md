<!--- vim: set et sw=2 ts=2 tw=80 : -->

# UniversalPizzoccheri

Text and client-server socket based idle game where you sell 
[Pizzoccheri](https://en.wikipedia.org/wiki/Pizzoccheri) 
in order to pay my university tuition fee.

## Italian warning

Although most of the source code is in english, this project contains hardcoded
italian strings. *Use at your own risk*.

## Building from source
- **client**: make client
- **server**: make server
- **cleaning leftowers from previous builds**: make clean

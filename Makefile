HEADERS = gamedata.h

ODIR = out
_OBJ_S = gamedata.o server.o
OBJ_S = $(patsubst %,$(ODIR)/%,$(_OBJ_S))

_OBJ_C = client.o
OBJ_C = $(patsubst %,$(ODIR)/%,$(_OBJ_C))

TARGET = out/server
TARGET_C = out/client

CC = gcc
CFLAGS = -g -Wall

default: client server

$(ODIR)/%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

server: $(OBJ_S)
	$(CC) $(OBJ_S) -o $(TARGET)

client: $(OBJ_C)
	$(CC) $(OBJ_C) -o $(TARGET_C)

clean:
	-rm -f $(OBJ_S) $(OBJ_C) $(TARGET) $(TARGET_C)
